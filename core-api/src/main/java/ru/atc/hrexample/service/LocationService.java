package ru.atc.hrexample.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.atc.hrexample.LocationDto;
import ru.atc.hrexample.LocationFilter;

/**
 * Севрис для работы с локациями
 */
public interface LocationService {

    /**
     * Получить локацию по идентификатору
     *
     * @param id идентификатор
     * @return модель с данными локации
     */
    LocationDto getLocationById(Integer id);

    /**
     * Записывает локацию в бд
     *
     * @param dto модель с данными локациями
     * @return модель с проставленным идентификатором
     */
    LocationDto create(LocationDto dto);

    /**
     * Обновляет локацию в бд
     *
     * @param dto данные для обновления
     * @return обновленная модель с данными
     */
    LocationDto update(LocationDto dto);

    /**
     * Получить список локаций по фильтру постранично
     *
     * @param filter фильтр для поиска
     * @param pageable настройки страницы
     * @return страница локаций
     */
    Page<LocationDto> getLocationsByFilter(LocationFilter filter, Pageable pageable);
}
