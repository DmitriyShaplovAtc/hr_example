package ru.atc.hrexample.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import ru.atc.hrexample.LocationFilter;
import ru.atc.hrexample.entity.LocationEntity;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface LocationMapper {

    int deleteByPrimaryKey(Integer locationId);

    int insert(LocationEntity record);

    int insertSelective(LocationEntity record);

    Optional<LocationEntity> selectByPrimaryKey(Integer locationId);

    int updateByPrimaryKeySelective(LocationEntity record);

    int updateByPrimaryKey(LocationEntity record);

    List<LocationEntity> selectPaged(@Param("filter") LocationFilter filter,
                                     @Param("pageable") Pageable pageable);

    long getTotalCount(@Param("filter") LocationFilter filter);
}