package ru.atc.hrexample.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.atc.hrexample.LocationDto;
import ru.atc.hrexample.LocationFilter;
import ru.atc.hrexample.service.LocationService;
import ru.atc.hrexample.validation.Update;

import javax.validation.groups.Default;

/**
 * REST-controller локаций
 */
@RestController
@RequestMapping("/locations")
@Tag(name = "locations", description = "API locations")
public class LocationController {

    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить локацию по идентификатору")
    public LocationDto getLocationById(@PathVariable Integer id) {
        return locationService.getLocationById(id);
    }

    @PostMapping
    @Operation(summary = "Создать локацию")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Создана локация",
                    content = @Content(schema = @Schema(implementation = LocationDto.class))),
            @ApiResponse(responseCode = "400", description = "Невалидные параметры",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера",
                    content = @Content) })
    public LocationDto createLocation(@Validated @RequestBody LocationDto dto) {
        return locationService.create(dto);
    }

    @PutMapping
    @Operation(summary = "Обновить локацию")
    public LocationDto updateLocation(@Validated({Update.class, Default.class}) @RequestBody LocationDto dto) {
        return locationService.update(dto);
    }

    @PostMapping("/search")
    @Operation(summary = "Получить локации с пагинацией")
    public Page<LocationDto> getLocationsPaged(@RequestBody LocationFilter filter,
                                               @ParameterObject Pageable pageable) {
        return locationService.getLocationsByFilter(filter, pageable);
    }
}
