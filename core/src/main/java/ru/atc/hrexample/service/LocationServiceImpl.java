package ru.atc.hrexample.service;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.atc.hrexample.LocationDto;
import ru.atc.hrexample.LocationFilter;
import ru.atc.hrexample.entity.LocationEntity;
import ru.atc.hrexample.repository.LocationMapper;

import java.util.List;

/**
 * Реализация сервиса по работе с локациями
 */
@Service
@Slf4j
public class LocationServiceImpl implements LocationService {

    private final LocationMapper locationMapper;
    private final ModelMapper modelMapper;

    public LocationServiceImpl(LocationMapper locationMapper, ModelMapper modelMapper) {
        this.locationMapper = locationMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public LocationDto getLocationById(Integer id) {
        LocationEntity entity = locationMapper.selectByPrimaryKey(id)
                .orElseThrow(() -> new RuntimeException(
                        String.format("Не найдена локация с идентификатором: %s", id)));
        return modelMapper.map(entity, LocationDto.class);
    }

    @Override
    @Transactional
    public LocationDto create(LocationDto dto) {
        LocationEntity entity = modelMapper.map(dto, LocationEntity.class);
        locationMapper.insert(entity);
        return getLocationById(entity.getLocationId());
    }

    @Override
    @Transactional
    public LocationDto update(LocationDto dto) {
        LocationEntity entity = modelMapper.map(dto, LocationEntity.class);
        locationMapper.updateByPrimaryKeySelective(entity);
        return getLocationById(entity.getLocationId());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LocationDto> getLocationsByFilter(LocationFilter filter, Pageable pageable) {
        List<LocationEntity> locationEntities = locationMapper.selectPaged(filter, pageable);
        List<LocationDto> resultDtos = modelMapper.map(locationEntities, new TypeToken<List<LocationDto>>() {
        }.getType());
        return new PageImpl<>(resultDtos, pageable, locationMapper.getTotalCount(filter));
    }
}
