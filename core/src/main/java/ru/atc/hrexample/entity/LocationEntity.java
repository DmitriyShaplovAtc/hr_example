package ru.atc.hrexample.entity;

import lombok.Data;

@Data
public class LocationEntity {
    private Integer locationId;
    private String streetAddress;
    private String city;
}