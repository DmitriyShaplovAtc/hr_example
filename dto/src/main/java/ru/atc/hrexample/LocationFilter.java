package ru.atc.hrexample;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Фильтр для локаций
 */
@Data
@Schema(title = "Фильтр для локаций")
public class LocationFilter {
    @Schema(title = "Адрес")
    private String streetAddress;
    @Schema(title = "Город")
    private String city;
}
