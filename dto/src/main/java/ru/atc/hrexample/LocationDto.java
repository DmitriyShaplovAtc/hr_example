package ru.atc.hrexample;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.atc.hrexample.validation.Update;

import javax.validation.constraints.NotNull;

/**
 * Модель данных локации
 */
@Data
@Schema(title = "Модель данных локации")
public class LocationDto {

    @Schema(title = "Идентификатор локации")
    @NotNull(groups = Update.class)
    private Integer locationId;

    @Schema(title = "Адрес")
    private String streetAddress;

    @Schema(title = "Город")
    @NotNull
    private String city;
}
